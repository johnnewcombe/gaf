﻿using System.Reflection;
using System.Runtime.CompilerServices;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.


// Note:
// The csproj file was modified in order for this file to be used. the property group as shown below was added.

// <PropertyGroup>
//    <GenerateAssemblyInfo>false</GenerateAssemblyInfo>
// </PropertyGroup> 

[assembly: AssemblyDescription("A simple to use GA framework for .Net Core 2.0")]
[assembly: AssemblyProduct("GAF (.Net Core 2.0)")]