﻿using System;
namespace GAF.Api
{
	public class Chromosome
	{
		public Chromosome (string data, double fitness, bool isElite)
		{
			Data = data;
			Fitness = fitness;
			IsElite = isElite;
		}

		public string Data { get; private set; }
		public double Fitness { get; private set; }
		public bool IsElite { get; private set; }
	}
}
