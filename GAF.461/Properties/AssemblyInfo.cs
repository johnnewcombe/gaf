﻿using System.Reflection;
using System.Runtime.CompilerServices;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.

[assembly: AssemblyDescription ("A simple to use GA framework for .Net")]
[assembly: AssemblyProduct ("GAF (.Net 4.6.1)")]
