﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;

namespace GAF.Network
{
    public static class Utility
    {
        public static IPAddress GetLocalIPv4(NetworkInterfaceType type)
        {

            var iface = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault(i => i.NetworkInterfaceType == type &&
                                                                                        i.OperationalStatus == OperationalStatus.Up);
            if (iface == null)
            {
                throw new NullReferenceException("Unable to find Ethernet interface.");
            }

            IPInterfaceProperties adapterProperties = iface.GetIPProperties();

            var ip = adapterProperties.UnicastAddresses.SingleOrDefault((a) => a.Address.AddressFamily == AddressFamily.InterNetwork);

            if (ip == null) {
                throw new NullReferenceException(string.Format("Unable to find IP Address to bind to on interface {0}", iface.Name));
            }
            return ip.Address;
        }

        public static IPAddress BytesToIP(byte[] ip)
        {
            return new IPAddress(ip);
        }

        public static byte[] IPToBytes(string ip)
        {
            return IPAddress.Parse(ip).GetAddressBytes();
        }

        public static byte[] IPToBytes(IPAddress ip)
        {
            return ip.GetAddressBytes();
        }
    }
}
