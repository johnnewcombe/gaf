Latest Stable Version
=====================

The source within the master branch represents the latest stable version and reflects the version that is published in [Nuget.org](http://nuget.org/packages/gaf). Other branches of code, if present, are subject to ongoing development and should be avoided.

Description
===========

The Genetic Algorithm Framework (GAF) is designed to be the simplest way to implement a genetic algorithm in C#. The GAF is a .Net/Mono assembly (.Net 4.5 - 4.7 and .Net Core 2.0), freely available via NuGet, that allows a genetic algorithm based solution to be implemented in C# using only a few lines of code.

For a full explanation of what a genetic algorithm is, and what it can do, please see the post Understanding Genetic Algorithms in less than 10 minutes and John Newcombe's article Implementing Genetic Algorithms in C# on Code Project.

The GAF has been designed to be as generic and extensible as possible whilst at the same time being simple to use. The single assembly contains all that is needed for many combinations of genetic algorithm. Many popular genetic operators are provided within the GAF, in addition, external operators can be defined and added to the GA process pipeline as required by the consumer.

To get started, simply install the GAF using the NuGet Package Manager. Execute the following command at the Package Manager Console.

`PM> Install-Package GAF`

Repository Info
===============

* GAF.40 - GAF.47 - Are projects that import code from the GAF.Shared project. These are compiled against the specified framework e.g. GAF.451 is compiled against .Net Framwework 4.5.1 etc.
* GAF.Core20 - This project imports code from the GAF.Shared project and builds against .Net Core 2.0.
* GAF.Pcl - This project imports code from the GAF.Shared project and builds against .Net PCL 4.5 Profile 78 (Windows 8+, Windows Phone 8+, Silverlight, Xamarin etc.). This portable library does not implement parallel task library operations for evaluiation.
* GAF.Api - This project provides an API for use by the GAF.LabGtk project.
* GAF.LabGtk - This Gtk based GUI Application presents a UI where GA settings can be manuipulated whilst the GA is running. 
* GAF.Shared - See above.
* GAF.EvaluationServer - This .Net 4.5 project creates a server executable designed to accept requests to evaluate solutions. This product forms part of a distributed evaluation approach that can be used with network funtionality (GAF.Network) and servicec discovery (GAF.Discovery). See [Wiki](https://bitbucket.org/johnnewcombe/gaf/wiki/) for details.
* GAF.Network - See GAF.EvaluationServer
* GAF.ServiceDiscovery - See GAF.EvaluationServer

Examples
--------
See [Wiki](https://bitbucket.org/johnnewcombe/gaf/wiki/) for details.

* Example.BinaryF6
* Example.TravellingSalesman
* Example.DistributedEvaluation
* Example.IFitness
* Example.IRemoteFitness

More Info
=========
For more information and repo contact details see [Wiki](https://bitbucket.org/johnnewcombe/gaf/wiki/).